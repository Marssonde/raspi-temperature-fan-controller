<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title><?php echo $site_name; ?></title>


    

    <!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="bootstrap-extras/dashboard.css" rel="stylesheet">
    <script src="bootstrap-extras/plotly-2.18.0.min.js"></script>
    <script src="bootstrap-extras/jquery.slim.min.js"></script>
  </head>
  <body>

  <?php include("./includes/nav.php"); ?>
    <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2"><?php echo $header_name; ?></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group mr-2">
            
<?php
$pages = [
  [
    'name' => 'http://192.168.200.181:9191',
    'linktext' => 'Raspberry Pi Camera',
    'icon' => 'home',
  ],
  [
    'name' => 'https://192.168.200.240:9191',
    'linktext' => 'Raspberry Pi Cloud',
    'icon' => 'thermometer',
  ],
];

foreach ($pages as $index => $page) :
  $listitem = "<a class='btn btn-sm btn-outline-secondary' href='".$page["name"] . $_SERVER["SCRIPT_NAME"]."'>";
  $listitem .= $page["linktext"] ."</a>";
  $pages[$index]["listitem"] = $listitem;
endforeach; ?>

 
    <?php foreach ($pages as $page) : echo $page["listitem"]; endforeach; ?>
          </div>
          
        </div>
      </div>
