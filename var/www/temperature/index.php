<?php 
$site_name = "Raspberry Pi Cloud";
$header_name = "Hardware-Informationen";
include ("./includes/header.php"); ?>



 <div class="row">   <div class="col-md-6">        
            <div class="card bg-light mb-3 ">
  <div class="card-header"><h5 class="card-title">Hardware-Informationen</h5></div>
  <div class="card-body">

                
    <pre class="card-text"><?php echo shell_exec("lshw | grep 'description:' -m 1");?>
    <?php echo shell_exec("lshw | grep 'product:' -m 1");?>    
     <?php echo shell_exec("lshw | grep 'width:' -m 1") . '</pre>';?>
  </div>
</div></div><hr>              <div class="col-md-6"><div class="card bg-light mb-3">
  <div class="card-header"><h5 class="card-title">Linux-Distribution und -Release</h5></div>
  <div class="card-body">

            <?php

                echo '    
    <pre class="card-text">' . shell_exec("hostnamectl") . '</pre>';
        ?>
  </div>
</div></div><hr>  <div class="col-md-6"><div class="card bg-light mb-3">
  <div class="card-header"><h5 class="card-title">Arbeitsspeicher</h5></div>
  <div class="card-body">

            <?php

                echo '    
    <pre class="card-text">' . shell_exec("free -m -t") . '</pre>';
        ?>
  </div>
</div></div><hr>   <div class="col-md-6"><div class="card bg-light mb-3">
  <div class="card-header"><h5 class="card-title">Laufwerke</h5></div>
  <div class="card-body">

            <?php

                echo '    
    <pre class="card-text">' . shell_exec("df -h") . '</pre>';
        ?>
  </div>
</div></div><hr>   <div class="col-md-12"><div class="card bg-light mb-3">
  <div class="card-header"><h5 class="card-title">laufende Prozesse</h5></div>
  <div class="card-body">

            <?php

                echo '
    <pre class="card-text">' . shell_exec("ps axjf") . '</pre>';
        ?>
  </div></div>
</div><hr></div>
    </main>


<?php include ("./includes/footer.php"); ?>
