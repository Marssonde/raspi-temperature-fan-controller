<?php 
$site_name = "Raspberry Pi Camera";
$header_name = "Temperatur";
include ("./includes/header.php"); ?>

<div class="conainer">
       
    <div class="row">   <div class="col">   
        
        <div class="btn-toolbar mb-2 mb-md-0">
          <?php
          $fan = file_get_contents("/var/www/temperature/fan-status");
          $fan = 550000;
          $fan=$fan/10000;
          $output = shell_exec("cat /sys/class/thermal/thermal_zone0/temp");

          $CPU_TEMP=$output/1000;
          echo '<span class="badge badge-success"><span data-feather="percent"></span>Fanspeed:'. $fan .'%</span>&nbsp;';
          echo '<span class="badge badge-danger"><span data-feather="thermometer"></span>Temperatur:'. $CPU_TEMP .'°C</span>&nbsp;';
          
          ?>
          <template>
  <div>
    <label for="datepicker-buttons">Date picker with optional footer buttons</label>
    <b-form-datepicker
      id="datepicker-buttons"
      today-button
      reset-button
      close-button
      locale="en"
    ></b-form-datepicker>
  </div>
</template>
</div>
<hr>
<div class="col">  
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-secondary" id="submit1">1 hour</button>
  <button type="button" class="btn btn-secondary" id="submit3">3 hours</button>
  <button type="button" class="btn btn-secondary" id="submit6">6 hours</button>
</div>
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-secondary" id="submit12">12 hours</button>
  <button type="button" class="btn btn-secondary" id="submit15">15 hours</button>
  <button type="button" class="btn btn-secondary" id="submit24">24 hours</button>
</div>
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-secondary" id="submit7d">7 days</button>
</div>
        </div></div>
      </div></div>
<hr>
<div class="conainer">
<div class="row">   <div class="col-md-12">   
<div class="card">
  <div class="card-header" id="header">
    CPU Temperatur - last 1 hour
  </div>
  <div class="card-body">


    <div  class="graph" style="width:100%;height:500px;" id="myDiv1h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv1h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv3h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv3h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv6h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv6h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv12h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv12h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv15h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv15h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv24h"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv24h2"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv7d"></div>
    <div  class="graph" style="width:100%;height:500px;" id="myDiv7d2"></div>
  </div>
</div>
  </div>
</div>
</div>

<script>
<?php include ("temperature1h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv1h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv1h2', data2, layout, {responsive: true});
</script>

<script>
<?php include ("temperature3h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv3h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv3h2', data2, layout, {responsive: true});
</script>
<script>
<?php include ("temperature6h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv6h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv6h2', data2, layout, {responsive: true});
</script>
<script>
<?php include ("temperature12h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv12h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv12h2', data2, layout, {responsive: true});
</script>
<script>
<?php include ("temperature15h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv15h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv15h2', data2, layout, {responsive: true});
</script>
<script>
<?php include ("temperature24h.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv24h', data1, layout, {responsive: true});
Plotly.newPlot('myDiv24h2', data2, layout, {responsive: true});
</script>
<script>
<?php include ("temperature7d.php"); ?>
var data1 = [trace1];
var data2 = [trace2];
var layout = {
		showlegend: true,
		legend: {"orientation": "h"}
	    };
Plotly.newPlot('myDiv7d', data1, layout, {responsive: true});
Plotly.newPlot('myDiv7d2', data2, layout, {responsive: true});
</script>

<script>
  $( ".graph" ).hide();
  $( "div#myDiv1h" ).show();
  $( "div#myDiv1h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 1 hour");
$( "button#submit1" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv1h" ).show();
  $( "div#myDiv1h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 1 hour");
});
$( "button#submit3" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv3h" ).show();
  $( "div#myDiv3h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 3 hours");
});
$( "button#submit6" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv6h" ).show();
  $( "div#myDiv6h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 6 hours");
});
$( "button#submit12" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv12h" ).show();
  $( "div#myDiv12h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 12 hours");
});

$( "button#submit15" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv15h" ).show();
  $( "div#myDiv15h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 15 hours");
});
$( "button#submit24" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv24h" ).show();
  $( "div#myDiv24h2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 24 hours");
});
$( "button#submit7d" ).click(function() {
  $( ".graph" ).hide();
  $( "div#myDiv7d" ).show();
  $( "div#myDiv7d2" ).show();
  $( "div#header" ).text("CPU Temperatur - last 7 days");
});
</script>
 </div>
 
    
<hr>
<?php
   $minpwm = file_get_contents("/var/www/temperature/pwmmin");
   $minpwm=$minpwm/10000;
   $maxpwm = file_get_contents("/var/www/temperature/pwmmax");
   $maxpwm=$maxpwm/10000;
   $mintemp = file_get_contents("/var/www/temperature/tempmin");
   $mintemp=$mintemp/1000;
   $maxtemp = file_get_contents("/var/www/temperature/tempmax");
   $maxtemp=$maxtemp/1000;
   $frequenz = file_get_contents("/var/www/temperature/frequenz");
?>  
      <div class="conainer">
<div class="row">   <div class="col-md-12">   
<div class="card">
  <div class="card-header">
    Fan Control
  </div>
  <div class="card-body">  
	  <div class="slidecontainer"><h3>Minimum PWM <span id="outputminpwm" class="badge badge-success">New</span></h3>
	  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">0&nbsp;&nbsp;</div>
    </div><input type="range" min="0" max="69" value="30" class="form-control slider" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" id="minimumpwm">  
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">69&nbsp;</div>
    </div>
</div>
</div> <hr>

	  <div class="slidecontainer"><h3>Maximum PWM <span id="outputmaxpwm" class="badge badge-success">New</span></h3>
  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">70&nbsp;</div>
    </div>
  <input type="range" min="70" max="100" value="50" class="form-control slider" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" id="maximumpwm">
<div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">100</div>
    </div>
</div>
</div> <hr>

	  <div class="slidecontainer"><h3>Minimum Temperature <span id="mintempvalue" class="badge badge-success">New</span></h3>
  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">25&nbsp;</div>
    </div>
  <input type="range" min="25" max="49" value="30" class="form-control slider" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" id="mintemp">
<div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">49&nbsp;</div>
    </div>
</div>
</div> <hr>

	  <div class="slidecontainer"><h3>Maximum Temperature <span id="maxtempvalue" class="badge badge-success">New</span></h3>
  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">50&nbsp;</div>
    </div>
  <input type="range" min="50" max="70" value="50" class="form-control slider" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon"  id="maxtemp">
<div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">70&nbsp;</div>
    </div>
</div>
</div> <hr>
	  <div class="slidecontainer"><h3>Frequenz <span id="frequenzvalue" class="badge badge-success">New</span></h3>
  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">10&nbsp;</div>
    </div>
  <input type="range" min="10" max="100" value="50" class="form-control slider" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon"  id="frequenz">
<div class="input-group-prepend">
      <div class="input-group-text" id="btnGroupAddon">100&nbsp;</div>
    </div>
</div>
</div> <hr>
  </div>
</div>
  </div>
</div>
</div><hr>

<script>

var sliderminpwm = document.getElementById("minimumpwm");
var outputminpwm = document.getElementById("outputminpwm");
var slidermaxpwm = document.getElementById("maximumpwm");
var outputmaxpwm = document.getElementById("outputmaxpwm");
var slidermintemp = document.getElementById("mintemp");
var outputmintemp = document.getElementById("mintempvalue");
var slidermaxtemp = document.getElementById("maxtemp");
var outputmaxtemp = document.getElementById("maxtempvalue");
var sliderfrequenz = document.getElementById("frequenz");
var outputfrequenz = document.getElementById("frequenzvalue");
sliderminpwm.value = <?php echo $minpwm; ?>;
slidermaxpwm.value = <?php echo $maxpwm; ?>;
slidermintemp.value = <?php echo $mintemp; ?>;
slidermaxtemp.value = <?php echo $maxtemp; ?>;
sliderfrequenz.value = <?php echo $frequenz; ?>;

outputminpwm.innerHTML = sliderminpwm.value; // Display the default slider value
outputmaxpwm.innerHTML = slidermaxpwm.value; // Display the default slider value
outputmaxtemp.innerHTML = slidermaxtemp.value; // Display the default slider value
outputmintemp.innerHTML = slidermintemp.value; // Display the default slider value
outputfrequenz.innerHTML = sliderfrequenz.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
sliderminpwm.oninput = function() {
  outputminpwm.innerHTML = this.value;
    var value = this.value * 10000;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xmlhttp.open("GET", "getminpwm.php?q=" + value, true);
    xmlhttp.send();
  
} 
// Update the current slider value (each time you drag the slider handle)
slidermaxpwm.oninput = function() {
  outputmaxpwm.innerHTML = this.value;
    var value = this.value * 10000;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xmlhttp.open("GET", "getmaxpwm.php?q=" + value, true);
    xmlhttp.send();
} 
// Update the current slider value (each time you drag the slider handle)
slidermintemp.oninput = function() {
  outputmintemp.innerHTML = this.value;
    var value = this.value * 1000;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xmlhttp.open("GET", "getmintemp.php?q=" + value, true);
    xmlhttp.send();
} 
// Update the current slider value (each time you drag the slider handle)
slidermaxtemp.oninput = function() {
  outputmaxtemp.innerHTML = this.value;
    var value = this.value * 1000;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xmlhttp.open("GET", "getmaxtemp.php?q=" + value, true);
    xmlhttp.send();
} 
// Update the current slider value (each time you drag the slider handle)
sliderfrequenz.oninput = function() {
  outputfrequenz.innerHTML = this.value;
    var value = this.value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
      }
    };
    xmlhttp.open("GET", "getfrequenz.php?q=" + value, true);
    xmlhttp.send();
} 
</script>

    </main>

  


<?php include ("./includes/footer.php"); ?>
