<?php // content="text/plain; charset=utf-8"
require_once ('opendatabase.php');
echo "var trace1 = {\n";
echo "x: [";
$i = 0;
foreach ($datetime6 as $value) {
    if ($i>0){
       echo ", '$value'";
    } else {
       echo "'$value'";
    }
    $i++;
}
echo "],\n";
echo "y: [";
$i = 0;
foreach ($temperature6 as $value) {
    if ($i>0){
       echo ", $value";
    } else {
       echo "$value";
    }
    $i++;
}
echo "],\n";
echo "  type: 'scatter',\n";
echo "  name:'Temperature',\n";
echo "  line: {color: 'red', width: 1, shape: 'spline'}\n";
echo "};\n";

echo "var trace2 = {\n";
echo "x: [";
$i = 0;
foreach ($datetime6 as $value) {
   if ($i>0){
       echo ", '$value'";
    } else {
       echo "'$value'";
    }
    $i++;
}
echo "],\n";
echo "y: [";
$i = 0;
foreach ($pulsmode6 as $value) {
    if ($i>0){
       echo ", $value";
    } else {
       echo "$value";
    }
    $i++;
}
echo "],\n";
echo "  type: 'scatter',\n";
echo "  name:'Pulsemode (PWM)',\n";
echo "  line: {color: 'green', width: 1}\n";
echo "};\n";

?>
