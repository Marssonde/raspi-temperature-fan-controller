<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="/"><?php echo $site_name; ?></a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
</nav>

<div class="container-md">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">

        <ul class="nav flex-column">


<?php
$pages = [
  [
    'name' => 'index',
    'linktext' => 'Dashboard',
    'icon' => 'home',
  ],
  [
    'name' => 'temperature',
    'linktext' => 'Temperature',
    'icon' => 'thermometer',
  ],
];

foreach ($pages as $index => $page) :
  $listitem = "<li class='nav-item'>";
  if ($_SERVER["SCRIPT_NAME"] == $page["name"] . ".php") :
    $listitem .= "<a class='nav-link active'>";
  else :
    $listitem .= "<a class='nav-link' href='" . $page["name"] . ".php'>";
  endif;
  $listitem .= "<span data-feather='". $page["icon"] ."'></span>" . $page["linktext"] . "</a></li>";
  $pages[$index]["listitem"] = $listitem;
endforeach; ?>

 
    <?php foreach ($pages as $page) : echo $page["listitem"]; endforeach; ?>
  
        </ul>

      </div>
    </nav>
 </div>
</div>
