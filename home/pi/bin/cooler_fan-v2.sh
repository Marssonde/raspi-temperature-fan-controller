#!/bin/bash
# Lüftersteuerung
temperaturepath="/var/www/temperature"
pwmminimum="$temperaturepath/pwmmin"
pwmmaximum="$temperaturepath/pwmmax"
tempmaximum="$temperaturepath/tempmax"
tempminimum="$temperaturepath/tempmin"
frequenz="$temperaturepath/frequenz"
THRESHOLD_MIN=$(cat $tempminimum)
THRESHOLD_MAX=$(cat $tempmaximum)
PULSEMODE_MIN=$(cat $pwmminimum)
PULSEMODE_MAX=$(cat $pwmmaximum)
frequency=$(cat $frequenz)
GPIO=12

#frequency=50
# Celsius * 1000
#THRESHOLD_MIN=30000 # niedrigste Temperatur, bei der der Lüfter auf PULSEMODE_MIN angeht (30°)
#THRESHOLD_MAX=50000 # höchste Temperatur, bei der der Lüfter auf PULSEMODE_MAX geht (50°)
#PULSEMODE_MIN=500000 # minimalste Pulsweite (hier 50%)
#PULSEMODE_MAX=1000000 # maximale Pulsweite (hier 100%)

#fanspeed=$(pigs gdc $GPIO)
CPU_TEMP=$(cat /sys/class/thermal/thermal_zone0/temp) # Temperatur der CPU

# Errechnung der linearen Funktion y = m*x +c
m=$(echo "scale=2;($PULSEMODE_MAX-$PULSEMODE_MIN)/($THRESHOLD_MAX-$THRESHOLD_MIN)/10" | bc)
# c = y - m*x
c=$(echo "scale=2;(($PULSEMODE_MAX/10)-($m*$THRESHOLD_MAX))/1000" | bc)
# y= m*x+c => der lineare Graph zur Berechnung der Pulsweite 
# 50 Grad Celsius entspricht 60% Fanspeed und 60 Grad Celsius entspricht 100% Fanspeed
tmppulsemode=$(echo "scale=3;($m*$CPU_TEMP/1000+$c)*10000" | bc)
# $tmppulsemode/1 => Umwandung in Integer Zahl ohne Nachkommastellen
pulsemode=$(echo "scale=0;$tmppulsemode/1" | bc) # Pulsweite auf errechneten Wert festlegen

# Wenn die CPU Temperatur größer oder gleich THRESHOLD_MAX (50°) ist, dann wird die
# Pulsweite auf PULSEMODE_MAX (100%) gesetzt
if [ $CPU_TEMP -ge $THRESHOLD_MAX ]
  then
    # Bei Maximum Temperatur 100% Pulsmode 
    pulsemode=$PULSEMODE_MAX

# Wenn die CPU Temperatur kleiner THRESHOLD_MIN (30°) ist, dann wird die
# Pulsweite auf 0 gesetzt, also der Lüfter ausgeschaltet:
elif [ $CPU_TEMP -le $THRESHOLD_MIN ] 
  then
#    Bei Minimum Temperatur den Lüfter ausschalten
    pulsemode=0
    frequency=0
fi

#    Errechneter Pulsmode in der Webseite anzeigen
if [ ! -f "/var/www/temperature/fan-status" ]
  then
    touch /var/www/temperature/fan-status
    sudo chown mars:www-data /var/www/temperature/fan-status
    sudo chmod 644 /var/www/temperature/fan-status
fi
pulsemode_old=$(cat /var/www/temperature/fan-status)
echo $pulsemode > /var/www/temperature/fan-status

pigs hp $GPIO $frequency $pulsemode #Errechneter Pulsmode an den Lüfter ausgeben
if [ ! -f "/var/www/temperature/main-v2.db" ] # SQLite Datenbank mit Temperatur- und Pulsdaten füttern
  then
  sqlite3 /var/www/temperature/main-v2.db <<EOF
  CREATE TABLE MAIN (ID INTEGER PRIMARY KEY AUTOINCREMENT, TIMESTAMP INTEGER ,TEMPERATURE INTEGER ,PULSEMODE INTEGER);
  .exit
EOF
  sudo chown mars:www-data /var/www/temperature/main-v2.db
  sudo chmod 644 /var/www/temperature/main-v2.db
fi
sqlite3 /var/www/temperature/main-v2.db <<EOF
INSERT INTO MAIN (TIMESTAMP,TEMPERATURE,PULSEMODE)
VALUES (strftime('%s','now'), $CPU_TEMP, $pulsemode );
.exit
EOF


#fi
