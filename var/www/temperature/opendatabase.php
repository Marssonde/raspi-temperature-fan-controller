<?php
   class MyDB extends SQLite3 {
      function __construct() {
         $this->open('main-v2.db');
      }
   }
   $db = new MyDB();
   if(!$db) {
      echo $db->lastErrorMsg();
   }
      $sql1 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-1 hour') AND strftime('%s','now');
EOF;
      $sql3 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-3 hour') AND strftime('%s','now');
EOF;
      $sql6 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-6 hour') AND strftime('%s','now');
EOF;
      $sql12 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-12 hour') AND strftime('%s','now');
EOF;
      $sql15 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-15 hour') AND strftime('%s','now');
EOF;
      $sql24 =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-24 hour') AND strftime('%s','now');
EOF;
      $sql7d =<<<EOF
      SELECT * from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now','-7 days') AND strftime('%s','now');
EOF;
   $fanstatus = "";
   $fan = file_get_contents("fan-status");
   if($fan == "0") {
      $fanstatus = " Fan is off.";
   } else {
      $fanstatus = " Fan is on";
   }
   $id1 = array();
   $timestamp1 = array();
   $datetime1 = array();
   $pmdatetime1 = array();
   $temperature1 = array();
   $pulsmode1 = array();
   $pulsmodetimestamp1 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql1);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id1[]  = $row['ID'];
      $timestamp1[] = $row['TIMESTAMP'];
      $temperature1[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp1[] = $row['TIMESTAMP'];
      $pulsmode1[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime1[] = date_format($date,"Y-m-d H:i:s");
   }
   $id3 = array();
   $timestamp3 = array();
   $datetime3 = array();
   $temperature3 = array();
   $pulsmode3 = array();
   $pulsmodetimestamp3 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql3);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id3[]  = $row['ID'];
      $timestamp3[] = $row['TIMESTAMP'];
      $temperature3[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp3[] = $row['TIMESTAMP'];
      $pulsmode3[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime3[] = date_format($date,"Y-m-d H:i:s");
   }
   $id6 = array();
   $timestamp6 = array();
   $datetime6 = array();
   $temperature6 = array();
   $pulsmode6 = array();
   $pulsmodetimestamp6 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql6);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id6[]  = $row['ID'];
      $timestamp6[] = $row['TIMESTAMP'];
      $temperature6[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp6[] = $row['TIMESTAMP'];
      $pulsmode6[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime6[] = date_format($date,"Y-m-d H:i:s");
   }
      $id12 = array();
   $timestamp12 = array();
   $datetime12 = array();
   $temperature12 = array();
   $pulsmode12 = array();
   $pulsmodetimestamp12 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql12);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id12[]  = $row['ID'];
      $timestamp12[] = $row['TIMESTAMP'];
      $temperature12[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp12[] = $row['TIMESTAMP'];
      $pulsmode12[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime12[] = date_format($date,"Y-m-d H:i:s");
   }
      $id15 = array();
   $timestamp15 = array();
   $datetime15 = array();
   $temperature15 = array();
   $pulsmode15 = array();
   $pulsmodetimestamp15 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql15);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id15[]  = $row['ID'];
      $timestamp15[] = $row['TIMESTAMP'];
      $temperature15[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp15[] = $row['TIMESTAMP'];
      $pulsmode15[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime15[] = date_format($date,"Y-m-d H:i:s");
   }
      $id24 = array();
   $timestamp24 = array();
   $datetime24 = array();
   $temperature24 = array();
   $pulsmode24 = array();
   $pulsmodetimestamp24 = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql24);
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
      $id24[]  = $row['ID'];
      $timestamp24[] = $row['TIMESTAMP'];
      $temperature24[] = ($row['TEMPERATURE'] / 1000);
      $pulsmodetimestamp24[] = $row['TIMESTAMP'];
      $pulsmode24[] = ($row['PULSEMODE'] / 10000);
      $oldpulsmode = ($row['PULSEMODE'] / 10000);
      $date = date_create();
      date_timestamp_set($date,$row['TIMESTAMP']);
      $datetime24[] = date_format($date,"Y-m-d H:i:s");
   }
   $id7d = array();
   $timestamp7d = array();
   $datetime7d = array();
   $temperature7d = array();
   $pulsmode7d = array();
   $pulsmodetimestamp7d = array();
   $oldpulsmode = -1;
   $ret = $db->query($sql7d);
   $i = 0;
   while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
        $id7d[]  = $row['ID'];
        $timestamp7d[] = $row['TIMESTAMP'];
        $temperature7d[] = ($row['TEMPERATURE'] / 1000);
        $pulsmodetimestamp7d[] = $row['TIMESTAMP'];
        $pulsmode7d[] = ($row['PULSEMODE'] / 10000);
        $oldpulsmode = ($row['PULSEMODE'] / 10000);
        $date = date_create();
        date_timestamp_set($date,$row['TIMESTAMP']);
        $datetime7d[] = date_format($date,"Y-m-d H:i:s");
   }
   $db->close();
?>
